/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author estudiante
 */
public class Equipo {
    
    private String codEquipo;
    private String nomEquipo;
    private Jugador capitan;
    private String golesAFavor;
    private String golesEnContra;
    private String tarjetasRojas;
    private String tarjetasAmarillas;

    public Equipo() {
    }

    public Equipo(String codEquipo, String nomEquipo) {
        this.codEquipo = codEquipo;
        this.nomEquipo = nomEquipo;
    }

    public Equipo(String codEquipo, String nomEquipo, Jugador capitan, String golesAFavor, String golesEnContra, String tarjetasRojas, String tarjetasAmarillas) {
        this.codEquipo = codEquipo;
        this.nomEquipo = nomEquipo;
        this.capitan = capitan;
        this.golesAFavor = golesAFavor;
        this.golesEnContra = golesEnContra;
        this.tarjetasRojas = tarjetasRojas;
        this.tarjetasAmarillas = tarjetasAmarillas;
    }

    public String getCodEquipo() {
        return codEquipo;
    }

    public void setCodEquipo(String codEquipo) {
        this.codEquipo = codEquipo;
    }

    public String getNomEquipo() {
        return nomEquipo;
    }

    public void setNomEquipo(String nomEquipo) {
        this.nomEquipo = nomEquipo;
    }

    public Jugador getCapitan() {
        return capitan;
    }

    public void setCapitan(Jugador capitan) {
        this.capitan = capitan;
    }

    public String getGolesAFavor() {
        return golesAFavor;
    }

    public void setGolesAFavor(String golesAFavor) {
        this.golesAFavor = golesAFavor;
    }

    public String getGolesEnContra() {
        return golesEnContra;
    }

    public void setGolesEnContra(String golesEnContra) {
        this.golesEnContra = golesEnContra;
    }

    public String getTarjetasRojas() {
        return tarjetasRojas;
    }

    public void setTarjetasRojas(String tarjetasRojas) {
        this.tarjetasRojas = tarjetasRojas;
    }

    public String getTarjetasAmarillas() {
        return tarjetasAmarillas;
    }

    public void setTarjetasAmarillas(String tarjetasAmarillas) {
        this.tarjetasAmarillas = tarjetasAmarillas;
    }

    

    @Override
    public String toString() {
        return "Equipo{" + "codEquipo=" + codEquipo + ", nomEquipo=" + nomEquipo + ", capitan=" + capitan + ", golesAFavor=" + golesAFavor + ", golesEnContra=" + golesEnContra + ", tarjetasRojas=" + tarjetasRojas + ", tarjetasAmarillas=" + tarjetasAmarillas + '}';
    }
            
    
    
    
    
}
