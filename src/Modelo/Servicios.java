/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import control.Conexion;
import Modelo.Equipo;
import Modelo.Jugador;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author estudiante
 */
public class Servicios {

    public boolean existeJugador(String cedula) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para buscar si existe un jugador en la base de datos");

            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM jugador WHERE cedula = ?");
            consulta.setString(1, cedula);

            ResultSet rs = consulta.executeQuery();
            if (rs.next()) {
                System.out.println("Jugador encontrado en la base de datos");
                return true;
            }

            System.out.println("Jugador no encontrado en la base de datos");
            System.out.println("*****************************************");
            return false;
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Servicios.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public boolean existeEquipo(String codEquipo) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para buscar si existe un equipo en la base de datos");

            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM equipo WHERE codEquipo = ?");
            consulta.setString(1, codEquipo);

            ResultSet rs = consulta.executeQuery();
            if (rs.next()) {
                System.out.println("Equipo encontrado en la base de datos");
                return true;
            }

            System.out.println("Equipo no encontrado en la base de datos");
            System.out.println("*****************************************");
            return false;
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Servicios.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public void agregarJugador(Jugador j) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para insertar un jugador en la DB");
            String sql = "INSERT INTO `jugador`(`cedula`, `nombre`, `dorsal`, `posicion`) "
                    + "VALUES (?,?,?,?)";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, j.getId());
            consulta.setString(2, j.getNombre());
            consulta.setString(3, j.getDorsal());
            consulta.setString(4, j.getPosicion());

            consulta.executeUpdate();

            System.out.println("Jugador agregado con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public void agregarEquipo(Equipo eq) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para insertar un equipo en la base de datos");
            String sql = "INSERT INTO `equipo`(`codEquipo`,`nomEquipo`) "
                    + "VALUES (?,?)";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, eq.getCodEquipo());
            consulta.setString(2, eq.getNomEquipo());

            consulta.executeUpdate();

            System.out.println("Equipo agregado con exito");
            System.out.println("*****************************************");
        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public Jugador getJugador(String cedula) throws SQLException, ClassNotFoundException {

        Jugador j = new Jugador();
        try {
            System.out.println("Realizando consulta para obtener un jugador de la base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `jugador` WHERE cedula = ?");
            consulta.setString(1, cedula);

            ResultSet rs = consulta.executeQuery();

            while (rs.next()) {

                if (rs.getString(1).equals(cedula)) {

                    j.setId(rs.getString("cedula"));
                    j.setNombre(rs.getString("nombre"));
                    j.setDorsal(rs.getString("dorsal"));
                    j.setPosicion(rs.getString("posicion"));
                    System.out.println("Jugador extraido");
                }
            }

        } catch (SQLException e) {
            System.out.println("No se encontró un jugador con esta cedula");
            throw new SQLException(e);
        }
        return j;
    }
    
    public String getJugadorTXT (String cedula) throws SQLException, ClassNotFoundException {
        
        String output = "";
        
        try {
            System.out.println("Realizando consulta para obtener un jugador de la base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `jugador` WHERE cedula = ?");
            consulta.setString(1, cedula);

            ResultSet rs = consulta.executeQuery();

            while (rs.next()) {

                if (rs.getString(1).equals(cedula)) {
                    
                    output = "Cedula del jugador: " + rs.getString("cedula") + "\n"
                            + "Nombre del Jugador: " + rs.getString("nombre") + "\n"
                            + "Dorsal del Jugador: " + rs.getString("dorsal") + "\n"
                            + "Posicion del Jugador: " + rs.getString("posicion") + "\n";
                    
                    Equipo eq = new Equipo();
                    String idEq = rs.getString("id_equipo");
                            
                    eq = getEquipo(idEq);
                    
                    output += "Equipo para el que juega: " + eq.getNomEquipo();
                    System.out.println("Jugador extraido");
                }
            }

        } catch (SQLException e) {
            System.out.println("No se encontró un jugador con esta cedula");
            throw new SQLException(e);
        }
        return output;
    }

    public ArrayList<Equipo> getEquipos() throws SQLException, ClassNotFoundException {

        ArrayList<Equipo> listaEquipos = new ArrayList<>();

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para obtener todos los equipos de la Base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `equipo`");

            ResultSet rs = consulta.executeQuery();

            Jugador j = new Jugador();
            int i = 1;
            while (rs.next()) {
                System.out.println("Extrayendo equipo #" + i);
                Equipo eq = new Equipo();
                eq.setCodEquipo(rs.getString("codEquipo"));
                eq.setNomEquipo(rs.getNString("nomEquipo"));
                String cedula = rs.getString("capitan");
                j = getJugador(cedula);

                eq.setCapitan(j);
                eq.setGolesAFavor(rs.getString("golesAFavor"));
                eq.setGolesEnContra(rs.getString("golesEnContra"));
                eq.setTarjetasRojas(rs.getString("tarjetasRojas"));
                eq.setTarjetasAmarillas(rs.getString("tarjetasAmarillas"));

                listaEquipos.add(eq);
                i++;
            }

            System.out.println("Equipos obtenidos con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            System.out.println("Error al obtener equipos");
            throw new SQLException(e);
        }
        return listaEquipos;
    }

    public ArrayList<Jugador> getJugadores() throws SQLException, ClassNotFoundException {

        ArrayList<Jugador> listaJugadores = new ArrayList<>();

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para obtener todos los jugadores de la Base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `jugador`");

            ResultSet rs = consulta.executeQuery();

            int i = 1;
            while (rs.next()) {
                System.out.println("Extrayendo jugador #" + i);
                Jugador j = new Jugador();
                j.setId(rs.getString("cedula"));
                j.setNombre(rs.getString("nombre"));
                j.setDorsal(rs.getString("dorsal"));
                j.setPosicion("posicion");
                listaJugadores.add(j);
                i++;
            }

            System.out.println("Jugadores obtenidos con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            System.out.println("Error al obtener jugadores");
            throw new SQLException(e);
        }
        return listaJugadores;
    }

    public boolean isEquipoAsignado(String idEquipo, String cedula) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para saber si un jugador tiene un equipo asignado:");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM jugador WHERE cedula = ? AND id_equipo = ?");
            consulta.setString(1, cedula);
            consulta.setString(2, idEquipo);

            ResultSet rs = consulta.executeQuery();
            if (rs.next()) {
                System.out.println("El jugador ya tiene un equipo asignado");
                System.out.println("*****************************************");
                return true;
            }
            System.out.println("El jugador no tiene un equipo asignado");
            System.out.println("*****************************************");
            return false;
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Servicios.class.getName()).log(Level.SEVERE, null, e);
        }
        System.out.println("El jugador no tiene un equipo asignado");
        System.out.println("*****************************************");
        return false;
    }

    public void asignarEquipo(String codEquipo, String cedula) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para asignar un equipo a un jugador:");
            String sql = "UPDATE jugador SET id_equipo=? WHERE cedula = ?";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, codEquipo);
            consulta.setString(2, cedula);

            consulta.executeUpdate();

            System.out.println("Equipo asignado con exito");
            System.out.println("*****************************************");
        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public void borrarJugador(String cedula) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para borrar un jugador de la base de datos");
            String sql = "DELETE FROM jugador WHERE cedula = ?";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, cedula);
            consulta.executeUpdate();

            System.out.println("jugador eliminado con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public void actualizarJugador(Jugador j, String cedulaAntigua, String idNuevoEquipo) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Consulta para actualizar jugador");
            String sql = "UPDATE jugador SET cedula = ?, nombre = ?, dorsal = ?, posicion = ?, id_equipo = ? WHERE cedula = ?";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, j.getId());
            consulta.setString(2, j.getNombre());
            consulta.setString(3, j.getDorsal());
            consulta.setString(4, j.getPosicion());
            consulta.setString(5, idNuevoEquipo);
            consulta.setString(6, cedulaAntigua);

            consulta.executeUpdate();

            System.out.println("Datos actualizados con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public boolean isCapitan(String codEquipo, String capitan) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para saber si un jugador ya fue asignado como capitan:");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM equipo WHERE codEquipo = ? AND capitan = ?");
            consulta.setString(1, codEquipo);
            consulta.setString(2, capitan);

            ResultSet rs = consulta.executeQuery();
            if (rs.next()) {
                System.out.println("El jugador ya fue asignado como capitan");
                System.out.println("*****************************************");
                return true;
            }
            System.out.println("El jugador no ha sido asignado como capitan");
            System.out.println("*****************************************");
            return false;
        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Servicios.class.getName()).log(Level.SEVERE, null, e);
        }
        System.out.println("El jugador no ha sido asignado como capitan");
        System.out.println("*****************************************");
        return false;
    }

    public void asignarCapitan(String codEquipo, String cedula) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para asignar un capitan a un Equipo:");
            String sql = "UPDATE equipo SET capitan=? WHERE codEquipo = ?";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, cedula);
            consulta.setString(2, codEquipo);

            consulta.executeUpdate();

            System.out.println("Equipo asignado con exito");
            System.out.println("*****************************************");
        } catch (SQLException e) {
            throw new SQLException(e);
        }

    }

    public void ingresarDatosEquipo(String codEquipo, String tarjetasAmarillas, String tarjetasRojas, String golesAFavor, String golesEnContra) throws SQLException, ClassNotFoundException {

        try {
            System.out.println("*****************************************");
            System.out.println("Consulta para actualizar jugador");
            String sql = "UPDATE equipo SET golesAFavor = ?, golesEnContra = ?, tarjetasRojas = ?, tarjetasAmarillas = ? WHERE codEquipo  = ?";
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement(sql);

            consulta.setString(1, golesAFavor);
            consulta.setString(2, golesEnContra);
            consulta.setString(3, tarjetasRojas);
            consulta.setString(4, tarjetasAmarillas);
            consulta.setString(5, codEquipo);

            consulta.executeUpdate();

            System.out.println("Datos actualizados con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    public Equipo getEquipo(String codEquipo) throws SQLException, ClassNotFoundException {

        Equipo eq = new Equipo();

        try {
            System.out.println("*****************************************");
            System.out.println("Realizando consulta para obtener UN equipo de la Base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `equipo` WHERE codEquipo = ?");
            consulta.setString(1, codEquipo);

            ResultSet rs = consulta.executeQuery();

            while (rs.next()) {

                if (rs.getString(1).equals(codEquipo)) {

                    Jugador j = new Jugador();
                    eq.setCodEquipo(rs.getString("codEquipo"));
                    eq.setNomEquipo(rs.getNString("nomEquipo"));
                    String cedula = rs.getString("capitan");
                    
                    System.out.println("Obteniendo datos del capitan: ");
                    j = getJugador(cedula);

                    eq.setCapitan(j);
                    

                    eq.setGolesAFavor(rs.getString("golesAFavor"));
                    eq.setGolesEnContra(rs.getString("golesEnContra"));
                    eq.setTarjetasRojas(rs.getString("tarjetasRojas"));
                    eq.setTarjetasAmarillas(rs.getString("tarjetasAmarillas"));
                    
                }
            }

            System.out.println("" + eq.toString());
            System.out.println("Equipo obtenido con exito");
            System.out.println("*****************************************");

        } catch (SQLException e) {
            System.out.println("Error al obtener equipos");
            throw new SQLException(e);
        }
        return eq;
    }

}
