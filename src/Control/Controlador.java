/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Modelo.Equipo;
import Modelo.Jugador;
import Modelo.Servicios;
import Vista.Ver;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author estudiante
 */
public class Controlador implements ActionListener {

    Ver vista;
    Servicios ser;

    public Controlador(Ver v, Servicios ser) {
        super();
        this.vista = v;
        this.ser = ser;
        actionListener(this);
    }

    private void actionListener(ActionListener evt) {
        vista.btnGuardarJugador.addActionListener(evt);
        vista.btnLimpiarJugador.addActionListener(evt);
        vista.btnObtenerEquipos.addActionListener(evt);
        vista.btnBuscarJugador.addActionListener(evt);
        vista.btnAsignarEquipo.addActionListener(evt);
        vista.btnEliminarJugador.addActionListener(evt);
        vista.btnActualizarJugador.addActionListener(evt);
        vista.btnObtenerDatosJugador.addActionListener(evt);
        vista.btnCrearEquipo.addActionListener(evt);
        vista.btn0btenerEquipos.addActionListener(evt);
        vista.btnObtenerJugadores.addActionListener(evt);
        vista.btnAsignarCapitan.addActionListener(evt);
        vista.btnIngresarDatosEquipo.addActionListener(evt);
        vista.btnLimpiarEquipo.addActionListener(evt);
        vista.btnObtenerDatosEquipo.addActionListener(evt);
        //ME FALTÓ AAAAAA
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        try {

            if (evt.getActionCommand().contentEquals("Guardar Jugador")) {
                Servicios s = new Servicios();

                String id = vista.txtId.getText();
                String nombre = vista.txtNombre.getText();
                String dorsal = vista.txtDorsal.getText();
                String posicion = vista.txtPosicion.getText();

                if ("".equals(id) || "".equals(nombre) || "".equals(dorsal) || "".equals(posicion)) {

                    vista.txtAreaJugador.setText("Los campos de jugador estan vacios");

                } else if (s.existeJugador(id)) {
                    vista.txtAreaJugador.setText("El jugador ya se encuentra registrado en la DB");
                } else {
                    Jugador j = new Jugador(id, nombre, dorsal, posicion);
                    s.agregarJugador(j);
                    vista.txtAreaJugador.setText("Jugador registrado correctamente en la base de datos");
                }

            } else if (evt.getActionCommand().contentEquals("Limpiar Datos Jugador")) {

                vista.txtId.setText("");
                vista.txtNombre.setText("");
                vista.txtDorsal.setText("");
                vista.txtPosicion.setText("");

            } else if (evt.getActionCommand().contentEquals("Obtener Equipos")) {

                Servicios s = new Servicios();
                ArrayList<Equipo> listaEquipos;
                listaEquipos = s.getEquipos();

                vista.jComboBoxEquipos.removeAllItems();

                for (int i = 0; i < listaEquipos.size(); i++) {
                    vista.jComboBoxEquipos.addItem(listaEquipos.get(i).getNomEquipo());
                }

            } else if (evt.getActionCommand().contentEquals("Buscar")) {

                String cedula = vista.txtId.getText();

                Jugador j = new Jugador();
                Servicios s = new Servicios();

                if ("".equals(cedula)) {

                    vista.txtAreaJugador.setText("El campo cedula está vacio");

                } else if (s.existeJugador(cedula)) {

                    j = s.getJugador(cedula);
                    vista.txtId.setText(j.getId());
                    vista.txtNombre.setText(j.getNombre());
                    vista.txtDorsal.setText(j.getDorsal());
                    vista.txtPosicion.setText(j.getPosicion());
                    vista.txtAreaJugador.setText("Jugador encontrado en la base de datos");

                } else {
                    vista.txtAreaJugador.setText("El jugador no se encuenta en la base de datos");
                }

            } else if (evt.getActionCommand().contentEquals("Asignar Equipo")) {

                Servicios s = new Servicios();

                String equipoSeleccionado = (String) vista.jComboBoxEquipos.getSelectedItem();
                String cedula = vista.txtId.getText();
                String codEquipo = "";

                ArrayList<Equipo> listaEquipos;
                listaEquipos = s.getEquipos();

                for (int i = 0; i < listaEquipos.size(); i++) {
                    if (listaEquipos.get(i).getNomEquipo().equals(equipoSeleccionado)) {
                        codEquipo = listaEquipos.get(i).getCodEquipo();
                    }
                }

                if ("".equals(cedula)) {
                    vista.txtAreaJugador.setText("El campo de cedula está vacio");

                } else if (s.isEquipoAsignado(codEquipo, cedula)) {

                    vista.txtAreaJugador.setText("El jugador ya tiene un equipo asignado");
                } else {
                    s.asignarEquipo(codEquipo, cedula);

                    vista.txtAreaJugador.setText("El jugador con la cedula " + cedula + " fue asignado al equipo " + equipoSeleccionado);
                }

            } else if (evt.getActionCommand().contentEquals("Eliminar Jugador")) {

                Servicios s = new Servicios();
                String cedula = vista.txtId.getText();

                if ("".equals(cedula)) {
                    vista.txtAreaJugador.setText("El campo cedula está vacio");
                } else if (s.existeJugador(cedula)) {
                    s.borrarJugador(cedula);
                    vista.txtAreaJugador.setText("Jugador borrado con exito");
                } else {
                    vista.txtAreaJugador.setText("El jugador no se encuentra en la base de datos");
                }

            } else if (evt.getActionCommand().contentEquals("Actualizar Jugador")) {

                Servicios s = new Servicios();

                String cedula = vista.txtId.getText();

                if ("".equals(cedula)) {

                    vista.txtAreaJugador.setText("El campo cedula se encuenta vacio");

                } else if (s.existeJugador(cedula)) {

                    String id = vista.txtId.getText();
                    String nombre = vista.txtNombre.getText();
                    String dorsal = vista.txtDorsal.getText();
                    String posicion = vista.txtPosicion.getText();

                    Jugador j = new Jugador(id, nombre, dorsal, posicion);

                    ArrayList<Equipo> listaEquipos;
                    listaEquipos = s.getEquipos();

                    String equipoNuevoSelec = (String) vista.jComboBoxEquipos.getSelectedItem();
                    String codEquipoNuevo = "";
                    for (int i = 0; i < listaEquipos.size(); i++) {
                        if (listaEquipos.get(i).getNomEquipo().equals(equipoNuevoSelec)) {
                            codEquipoNuevo = listaEquipos.get(i).getCodEquipo();
                        }
                    }

                    s.actualizarJugador(j, cedula, codEquipoNuevo);

                }

            } else if (evt.getActionCommand().contentEquals("Obtener Datos Jugador")) {

                String busqCedula = vista.txtId.getText();
                Servicios s = new Servicios();

                if ("".equals(busqCedula)) {
                    vista.txtAreaJugador.setText("El campo cedula está vacio");
                } else if (s.existeJugador(busqCedula)) {

                    String salida = s.getJugadorTXT(busqCedula);

                    vista.txtAreaJugador.setText(salida);
                }
            } else if (evt.getActionCommand().contentEquals("Crear Equipo")) {

                Servicios s = new Servicios();
                String codEquipo = vista.txtCodEquipo.getText();
                String nomEquipo = vista.txtNomEquipo.getText();

                if ("".equals(codEquipo) || "".equals(nomEquipo)) {

                    vista.txtAreaEquipo.setText("El campo codigo equipo o nombre del equipo está vacio");

                } else if (s.existeEquipo(codEquipo)) {

                    vista.txtAreaEquipo.setText("El equipo ya se encuentra registrado en la base de datos");

                } else {

                    Equipo eq = new Equipo(codEquipo, nomEquipo);
                    System.out.println("" + eq.toString());
                    s.agregarEquipo(eq);
                    vista.txtAreaEquipo.setText("Equipo agregado correctamente a la base de datos");
                }

            } else if (evt.getActionCommand().contentEquals("Obtener Jugadores")) {
                Servicios s = new Servicios();
                ArrayList<Jugador> listaJugadores;
                listaJugadores = s.getJugadores();

                vista.jComboBoxJugadores.removeAllItems();

                for (int i = 0; i < listaJugadores.size(); i++) {
                    vista.jComboBoxJugadores.addItem(listaJugadores.get(i).getId());
                }
            } else if (evt.getActionCommand().contentEquals("0btener Equipos")) {

                Servicios s = new Servicios();
                ArrayList<Equipo> listaEquipos;
                listaEquipos = s.getEquipos();

                vista.jComboB0xEquipos.removeAllItems();

                for (int i = 0; i < listaEquipos.size(); i++) {
                    vista.jComboB0xEquipos.addItem(listaEquipos.get(i).getNomEquipo());
                }

            } else if (evt.getActionCommand().contentEquals("Asignar Capitan")) {

                String equipoSeleccion = (String) vista.jComboB0xEquipos.getSelectedItem();
                String cedulaSeleccion = (String) vista.jComboBoxJugadores.getSelectedItem();

                Servicios s = new Servicios();
                ArrayList<Jugador> listaJugadores;
                listaJugadores = s.getJugadores();
                ArrayList<Equipo> listaEquipos;
                listaEquipos = s.getEquipos();

                String codEquipo = "";
                for (int i = 0; i < listaEquipos.size(); i++) {
                    if (listaEquipos.get(i).getNomEquipo().equals(equipoSeleccion)) {
                        codEquipo = listaEquipos.get(i).getCodEquipo();
                    }
                }

                if ("Placeholder".equals(equipoSeleccion) || "Placeholder".equals(cedulaSeleccion)) {
                    vista.txtAreaEquipo.setText("No se ha seleccionado un equipo o un capitan");
                } else if (s.isCapitan(codEquipo, cedulaSeleccion)) {
                    System.out.println("El jugador ya fue asignado como capitan");
                } else {
                    s.asignarCapitan(codEquipo, cedulaSeleccion);
                }
            } else if (evt.getActionCommand().contentEquals("Ingresar datos Equipo")) {

                Servicios s = new Servicios();
                ArrayList<Equipo> listaEquipos;
                listaEquipos = s.getEquipos();

                String nomEquipo = (String) vista.jComboB0xEquipos.getSelectedItem();

                String codEquipo = "";
                for (int i = 0; i < listaEquipos.size(); i++) {
                    if (listaEquipos.get(i).getNomEquipo().equals(nomEquipo)) {
                        codEquipo = listaEquipos.get(i).getCodEquipo();
                    }
                }

                if ("Placeholder".equals(nomEquipo)) {
                    vista.txtAreaEquipo.setText("No se ha seleccionado un equipo o un capitan");
                } else {
                    String tarjetasAmarillas = vista.txtTarjetasAmarillas.getText();
                    String tarjetasRojas = vista.txtTarjetasRojas.getText();
                    String golesAFavor = vista.txtGolesAFavor.getText();
                    String golesEnContra = vista.txtGolesEnContra.getText();

                    s.ingresarDatosEquipo(codEquipo, tarjetasAmarillas, tarjetasRojas, golesAFavor, golesEnContra);
                }

            } else if (evt.getActionCommand().contentEquals("Limpiar Datos Equipo")) {

                vista.txtCodEquipo.setText("");
                vista.txtNomEquipo.setText("");
                vista.jComboB0xEquipos.removeAllItems();
                vista.jComboBoxJugadores.removeAllItems();
                vista.txtTarjetasAmarillas.setText("");
                vista.txtTarjetasRojas.setText("");
                vista.txtGolesAFavor.setText("");
                vista.txtGolesEnContra.setText("");

            } else if (evt.getActionCommand().contentEquals("Obtener Datos Equipo")) {

                Servicios s = new Servicios();
                Equipo e = new Equipo();

                String codEquipo = vista.txtCodEquipo.getText();

                if ("".equals(codEquipo)) {
                    vista.txtAreaEquipo.setText("El campo de codigo de equipo esta vacio");
                } else {
                    e = s.getEquipo(codEquipo);

                    String salida = "Codigo del equipo: " + e.getCodEquipo() + "\n"
                            + "Nombre del Equipo: " + e.getNomEquipo() + "\n"
                            + "*****Informacion del Capitan*****\n" + "ID del capitan: "
                            + e.getCapitan().getId() + "\n" + "Nombre del capitan: "
                            + e.getCapitan().getNombre() + "\n" + "***********************************"
                            + "\n" + "Goles A Favor: " + e.getGolesAFavor() + "\n" + "Goles En Contra: "
                            + e.getGolesEnContra() + "\n" + "Tarjetas Amarillas: " + e.getTarjetasRojas()
                            + "\n" + "Tarjetas Rojas: " + e.getTarjetasAmarillas();

                    vista.txtAreaEquipo.setText(salida);
                }
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

}
