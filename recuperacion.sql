-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-05-2022 a las 17:26:00
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `recuperacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `codEquipo` varchar(20) COLLATE utf8_bin NOT NULL,
  `nomEquipo` varchar(200) COLLATE utf8_bin NOT NULL,
  `capitan` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `golesAFavor` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `golesEnContra` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `tarjetasRojas` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `tarjetasAmarillas` varchar(30) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`codEquipo`, `nomEquipo`, `capitan`, `golesAFavor`, `golesEnContra`, `tarjetasRojas`, `tarjetasAmarillas`) VALUES
('122', 'ChePLACE', NULL, '5', '5', '5', '5'),
('123', 'Core', NULL, NULL, NULL, NULL, NULL),
('457', 'UWU', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

CREATE TABLE `jugador` (
  `cedula` varchar(100) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(200) COLLATE utf8_bin NOT NULL,
  `dorsal` varchar(200) COLLATE utf8_bin NOT NULL,
  `posicion` varchar(200) COLLATE utf8_bin NOT NULL,
  `id_equipo` varchar(20) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `jugador`
--

INSERT INTO `jugador` (`cedula`, `nombre`, `dorsal`, `posicion`, `id_equipo`) VALUES
('1246346', 'Felipe', '11', 'Volante', NULL),
('60390192', 'Adriana', '11', 'Delantera', '123'),
('88248', 'Edisson', '19', 'Delantero', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`codEquipo`),
  ADD KEY `capitan` (`capitan`);

--
-- Indices de la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`cedula`),
  ADD KEY `id_equipo` (`id_equipo`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`capitan`) REFERENCES `jugador` (`cedula`) ON DELETE SET NULL;

--
-- Filtros para la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD CONSTRAINT `jugador_ibfk_1` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`codEquipo`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
